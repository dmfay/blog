---
layout: Page
title: about me
---

It's an older spelling of Diane, if you were wondering how it's pronounced. Right now, I'm working in data architecture and backend development for [Beacon Biosignals](https://beacon.bio)' EEG analysis platform, wrangling a mix of PostgreSQL, Snowflake, GraphQL (with Postgraphile), Kubernetes, and TypeScript.

I also develop software and consult on data modeling and Postgres implementation as [Applied Poietics, LLC](https://applied-poietics.com).

Otherwise and previously:

- [MassiveJS](https://massivejs.org) - a moderately popular open source data mapper for NodeJS and PostgreSQL, and [monstrous](https://gitlab.com/monstrous/monstrous), its successor
- staff engineer at [Understory Weather](https://understoryweather.com), where I owned weather station logistics and traceability, maintained big data infrastructure and pipelines, and collaborated with hardware, field, and data science teams
- data architecture, API development, and process for accessibility testing & compliance software at [Deque Systems](https://www.deque.com)
- web and database development, plus a stint as team lead, at what's now [GTB](https://www.gtb.com)
- and more!
