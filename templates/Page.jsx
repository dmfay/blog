import Site from './Site';

export default function ({typo, title, content}) {
  return (
    <Site {...{title, lang: 'en'}}>
      <article>
        <h2>{title}</h2>
        <div>
          {typo(content)}
        </div>
      </article>
    </Site>
  );
};
