export default function ({title, summary, canonicalUrl, lang}, children) {
  return (
    <html lang={lang}>
      <head>
        <meta charset='utf-8' />
        <meta name='twitter:card' content='summary' />
        <meta name='twitter:creator' content='@dianmfay' />
        <meta property='og:title' content={title.toLowerCase()} />
        <meta property='og:description' content={summary} />
        <meta property='og:url' content={canonicalUrl} />
        <title>{title.toLowerCase()} | dian m fay</title>
        <link rel='stylesheet' href='/css/site.css' />
        <link href='https://fonts.googleapis.com/css?family=Work+Sans:300' rel='stylesheet' />
        <link href='https://fonts.googleapis.com/css?family=Wire+One' rel='stylesheet' />
      </head>
      <body>
        <header>
          <h1>dian m fay</h1>
          <ul class='links'>
            <li><a href='/'>home</a></li>
            <li><a href='/tags'>tags</a></li>
            <li><a href='/atom.xml'>rss</a></li>
            <li><a href='/about'>about</a></li>
            <li><a href='https://gitlab.com/dmfay'>gitlab</a></li>
            <li><a href='https://www.linkedin.com/in/dian-fay-7408375a/'>linkedin</a></li>
            <li><a href='mailto:di@nmfay.com'>email</a></li>
          </ul>
        </header>
        <div id='main' role='main'>
          {children}
        </div>
        <script src='/js/remix.js' />
      </body>
    </html>
  )
};
