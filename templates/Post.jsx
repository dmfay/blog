import Site from './Site';

export default function ({typo, title, summary, canonicalUrl, tags, content}) {
  const links = tags.map(t => {
    return (<li>
      <a href={`/tags/${t}`}>{t}</a>
    </li>)
  });

  return (
    <Site {...{title, summary, canonicalUrl, lang: 'en'}}>
      <article>
        <h2 style='margin-bottom: 0'>{title}</h2>
        <ul class='tags' style='text-align: right'>
          {links}
        </ul>
        <div id='post-content'>
          {typo(content)}
        </div>
      </article>

      <div class='container'>
        <div class='button-footer'>
          <button id='remix-button'>Remix</button>
        </div>
        <ul id='choices'></ul>
        <div id='output'></div>
      </div>

    </Site>
  );
};
