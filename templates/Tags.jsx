import Site from './Site';

export default function ({tags, typoTitle}) {
  const links = tags.sort().map(tag => {
    return (<li>
      <a href={`/tags/${tag}`}>{tag}</a>
    </li>);
  });

  return (
    <Site {...{title: 'tags', lang: 'en'}}>
      <article>
        <h2>tags</h2>
        <ul class='taglist'>
          {links}
        </ul>
      </article>
    </Site>
  );
};
