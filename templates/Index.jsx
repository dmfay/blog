import Site from './Site';

export default function ({index, tag, typoTitle, documents, documentsTotal, previousUrl, nextUrl}) {
  const links = documents.map(doc => {
    return (<li>
      <a href={doc.url}>{typoTitle(doc.title)}</a> ({doc.timestamp.format('MMM DD YYYY')})
    </li>);
  });

  let title, heading;

  if (tag) {
    title = index === 0 ? `Index | ${tag}` : `Page ${index + 1} | ${tag}`;
    heading = tag;
  } else {
    title = index === 0 ? 'Index' : `Page ${index + 1}`;
    heading = 'posts';
  }

  const prev = previousUrl ? (<a href={previousUrl}>prev</a>) : (<span>prev</span>);
  const next = nextUrl ? (<a href={nextUrl}>next</a>) : (<span>next</span>);

  return (
    <Site {...{title, lang: 'en'}}>
      <article>
        <h2>{heading}</h2>
        <ol class='posts'>
          {links}
        </ol>
        <ul class='pager'>
          <li>{prev}</li>
          <li>{next}</li>
          <li>[{documentsTotal} total]</li>
        </ul>
      </article>
    </Site>
  );
};

