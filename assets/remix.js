var submit = document.getElementById('remix-button');
var content = document.getElementById('post-content');
var choices = document.getElementById('choices');
var output = document.getElementById('output');

var prefixLength = 2;
var groups;

var setup = function () {
  if (!submit) {
    return;
  }

  submit.addEventListener('click', function (e) {
    e.preventDefault();

    output.innerText = '';

    var corpus = Array.prototype.slice.call(document.querySelectorAll('p')).map(function (node) { return node.innerText; }).join(' ');

    if (!corpus) { return false; }

    groups = corpus.split(/\s+/).reduce(function (map, word, idx, words) {
      if (idx >= words.length - prefixLength) { return map; }

      const group = words.slice(idx, idx + prefixLength + 1);
      const next = group.pop();
      const key = JSON.stringify(group);

      if (!map.hasOwnProperty(key)) {
        map[key] = new Set();
      }

      map[key].add(next);

      return map;
    }, {});

    var start = Object.keys(groups)[0];
    var parsed = JSON.parse(start);

    output.appendChild(document.createTextNode(parsed.join(' ') + ' '));

    cycle(start);
  });
};

var cycle = function (key) {
  var next = groups[key];

  key = JSON.parse(key);

  choices.innerHTML = '';

  if (next.size === 1) {
    var value = next.values().next().value;

    output.appendChild(document.createTextNode(value + ' '));

    key.shift();
    key.push(value);

    cycle(JSON.stringify(key));
  } else if (next.size > 1) {
    next.forEach(choice => {
      var el = document.createElement('li');
      var a = document.createElement('a');
      var text = document.createTextNode(choice);

      a.setAttribute('href', '#' + encodeURIComponent(choice));
      a.addEventListener('click', function (e) {
        var target = e.target;

        output.appendChild(document.createTextNode(target.innerText + ' '));

        key.shift();
        key.push(target.innerText);

        cycle(JSON.stringify(key));
      });

      a.appendChild(text);
      el.appendChild(a);
      choices.appendChild(el);
    });
  }

  window.scrollTo(0, document.body.scrollHeight);
}

if (document.readyState !== 'loading') { setup(); }
else { document.addEventListener('DOMContentLoaded', setup); }
