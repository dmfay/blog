import {
  start,
  loadConfig,
  loadSourceFiles,
  generatePages,
  savePages,
  orderDocuments,
  groupDocuments,
  paginate,
  createMarkdownRenderer,
  createTemplateRenderer,
  helpers
} from 'fledermaus';
import moment from 'moment';
import slug from 'remark-slug';
import align from 'remark-align';

const indexer = (page, idx) => {
  page.index = idx;

  return page;
};

start('Building site...');

const config = loadConfig('config');
const options = config.base;

const mdRenderer = createMarkdownRenderer({
  plugins: [slug, align]
});
const jsxRenderer = createTemplateRenderer({root: options.templatesFolder});

let posts = loadSourceFiles(options.postFolder, options.postExts, {
  renderers: {
    md: mdRenderer
  },
  fieldParsers: {
    timestamp: (date, attrs) => moment.utc(attrs.date, 'YYYY-MM-DD'),
    // invent a new canonicalUrl prop for the meta tags
    canonicalUrl: (u, attrs) => `${options.url}${attrs.url}`
  }
});

posts = orderDocuments(posts, ['-timestamp']);

const toBuild = posts.slice();

// pagination
toBuild.push(...paginate(posts, {
  sourcePathPrefix: '',
  urlPrefix: '/',
  documentsPerPage: options.pageLength,
  layout: 'Index',
  index: true
}).map(indexer));

// tagging
const byTag = groupDocuments(posts, 'tags');
const tags = Object.keys(byTag);
toBuild.push(...tags.reduce((acc, tag) => {
  const taggedPosts = byTag[tag];
  const tagPages = paginate(taggedPosts, {
    sourcePathPrefix: `/tags/${tag}`,
    urlPrefix: `/tags/${tag}`,
    documentsPerPage: options.pageLength,
    layout: 'Index',
    extra: {tag},
    index: true
  }).map(indexer);
  return [
    ...acc,
    ...tagPages,
    {
      sourcePath: `/tags/${tag}`,
      url: `/tags/${tag}`,
      layout: 'RSS',
      items: taggedPosts,
      title: `${config.base.title} - ${tag}`,
      description: config.base.description,
      copyright: config.base.author
    }
  ];
}, []));

toBuild.push({
  sourcePath: 'tags/index.html',
  url: '/tags/index.html',
  layout: 'Tags',
  tags
});

// RSS feed
toBuild.push({
  sourcePath: 'atom.xml',
  url: '/atom',
  layout: 'RSS',
  items: posts.slice(0, options.postsInFeed),
  title: config.base.title,
  description: config.base.description,
  copyright: config.base.author
});

const statics = loadSourceFiles(options.staticFolder, options.staticExts, {
  renderers: {
    md: mdRenderer
  }
});

toBuild.push(...statics);

const pages = generatePages(toBuild, config, helpers, {
  jsx: jsxRenderer
});

savePages(pages, options.publicFolder);
