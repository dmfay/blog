---
layout: Post
title: 'Summer 2018: Massive, Twice Over'
date: 2018-07-30
tags:
  - massive
  - speaking
  - architecture
  - database
---

[NDC talks are up](https://vimeo.com/281409168)!

There's also the [FullStack London](https://skillsmatter.com/skillscasts/12008-database-as-api-with-postgresql-and-massive-js) version which is slightly condensed for a shorter timeslot, if you have a SkillsMatter account and want to get right to the fun parts.

If you've read (almost) anything I've written, text or code, odds are you've run into [Massive.js](https://massivejs.org). On the off chance you haven't, the elevator pitch is that PostgreSQL exclusivity lets you get a lot more mileage out of your database (as long as it's Postgres) and JavaScript being a dynamically typed, functional-ish language lets you get away with it really easily.

This talk goes over Massive in much more depth: first laying out a case for alternatives to the dominant object-relational mapping data access technique, in general and especially in JavaScript; and then diving into the architecture of Massive itself with plenty of examples. Also, there's some trivia about early 20th century Russian avant-garde art and another bit poking fun at French modernist architect Le Corbusier.

It's the second talk I've done, and overall I was pretty happy with how it went in Oslo and London both! I'm the furthest thing from a natural public speaker but I covered what I wanted to cover, finished at a reasonable time, and didn't screw anything up too badly -- so that's a success in my book. And after all, the only way to improve this particular skill is to keep doing it.
