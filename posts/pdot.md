---
layout: Post
title: "pdot: Exploring Databases Visually, Part II"
summary: "graph database? graph _your_ database"
date: 2023-08-13
tags:
  - postgresql
  - database
  - shell
  - rust
---

A couple years ago, I wrote about [exploring a running database](https://di.nmfay.com/exploring-databases-visually) by plotting relevant subsets of the foreign key relationship graph in [dot](https://graphviz.org) and piping the resulting images directly to the terminal. [Things have progressed since then](https://gitlab.com/dmfay/pdot):

- I supplemented the original `fks` shell script with others plotting view dependencies, role hierarchies and grants, and finally started to map the effects of triggers and functions;
- I hit my personal ceiling of What It Is Reasonable To Do In Shell Scripts, and decided to pull all this stuff together into a single cross-platform program with a consistent interface;
- I did _that_, and added [mermaid](https://mermaid.js.org) support for good measure;
- & then, I forgot to write anything about having released it for a couple of months, as you do

The `fks` side of things hasn't changed much from the earlier post (aside from some niceties around table inheritance), so here's the big new thing:

![plot showing affected tables and further trigger cascades resulting from an `on_flight_delayed` trigger](/images/pdot-pgair-triggers-flight.png)

[pdot](https://gitlab.com/dmfay/pdot/-/releases) is out for Linux (including the [Arch AUR](https://aur.archlinux.org/packages/pdot-git)), Windows, and macOS universal. I'll be talking more about it and exploration as a documentation strategy at the [Chicago PUG](https://www.meetup.com/chicago-postgresql-user-group/) in November, and possibly elsewhere!
