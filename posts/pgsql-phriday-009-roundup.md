---
layout: Post
title: "PGSQL Phriday #009 Roundup"
summary: "surveying the aftermath"
date: 2023-06-07
tags:
  - postgresql
---

Another Phriday in the books, and it's time to see what all happened:

- [Hari Kiran](https://opensource-db.com) offered an [introduction to the concepts and processes used in schema evolution](https://opensource-db.com/database-change-management-pgsql-phriday-009/) with examples showing how to use Flyway, a popular Java-based tool.
- [Grant Fritchey](https://www.scarydba.com) discussed [what it means to roll a change back](https://www.scarydba.com/2023/06/02/pgsql-phriday-009-on-rollback/), why catastrophic failures are the _good_ kind of failure, and how to work with deployment processes to adapt to failures and roll the database forward to a good state instead of trying to turn back time.
- [Michael Christofides](https://www.pgmustard.com/blog) wrote about [table stakes for database automation](https://www.pgmustard.com/blog/database-change-management-vs-performance) and his hopes for better integration of performance testing in automated change management.
- [Andy Atkinson](https://andyatkinson.com/blog) ran through [the entire prompt item by item](https://andyatkinson.com/blog/2023/06/02/pgsql-phriday-009-schema-change-management)! Read it for a detailed look at Rails migrations _in situ_ -- who writes them, who reviews them, what kinds of problems happen, and how to validate successful changes.
- [Ryan Booz](https://www.softwareandbooz.com) gave me ERWin flashbacks and proclaimed a [decalogue for the aspiring database automator](https://www.softwareandbooz.com/10-requirements-for-managing-database-changes/), from the foundational on up. No comment on whether I've ever achieved #7 without painstakingly restoring manual production dumps.
- finally, I covered the [ideas powering a few less-usual schema evolution tools](https://di.nmfay.com/pgsql-phriday-three-big-ideas).

Thanks everyone for participating, and look forward to [Alicja's invitation](https://www.pgsqlphriday.com/calendar) coming around the end of the month!
