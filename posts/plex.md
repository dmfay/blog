---
layout: Post
title: 'Plex: A Life'
date: 2019-09-06
tags:
  - philosophy
  - history
  - biography
---

A little while back I got my hands on a copy of _Software Development and Reality Construction_, the output of a conference held in Berlin in 1988. Among a variety of other more or less philosophical treatments of the theory and practice of software development, Don Knuth analyzes errors he made in his work on TeX; Kristen "SIMULA" Nygaard reviews his collaboration with labor unions to ensure that software meant to coordinate and control work does not wind up controlling the workers as well, a rather grim read in the era of Uber and Amazon; Heinz Klein and Kalle Lyytinen embark on a discussion of data modeling as production rather than as interpretation or hermeneutics. In all, it's some of the most insightful writing about programming and software engineering I've encountered.

This isn't about those contributions.

There's an entry fairly early on from one Douglas T. Ross, called "From Scientific Practice to Epistemological Discovery". Ross, who died in 2007, was a computer scientist and engineer most remembered today for the influential APT machine tools programming language and for coining the term "computer-aided design".

This isn't about the things Doug Ross is remembered for.

Doug Ross had a _system_. The system began its public life as an early software engineering methodology in the Cambrian explosion of such methodologies enabled by the spread of high-level programming languages in the 60s and 70s. The system went by a few names. Ross's company, SofTech Inc., called it the Structured Analysis and Design Technique or SADT. The US Air Force, never wont to use merely one acronym where two will do, called it IDEF0: ICAM (Integrated Computer Aided Manufacturing) DEFinition for function modeling.

To Doug Ross, the system was Plex. And Plex was everything. When the Department of Defense cut the Structured Analysis data modeling approach from IDEF0 in favor of a simpler methodology to be developed by SofTech subcontracters and named IDEF1, Ross decried the decision as destroying the "mathematical elegance and symmetric completeness of SADT [...] IDEF0 became merely the best of a competing zoo of other software development CASE tools, none of which were scientifically founded". He saw his career, and, indeed, his life, as drawing him inevitably toward the discovery and promulgation of his "philosophy of problem-solving", and furthering Plex's development became more and more important to him as time went on. In the mid-80s, he stopped drawing a salary at SofTech and went back to MIT, lecturing part-time on electrical engineering in order to focus more of his efforts on Plex.

But even MIT was, in Ross's own words, "not yet ready for [Structured Analysis] much less Plex". A graduate seminar on Plex itself was briefly offered in 1984, but was canceled due to lack of student interest. In "From Scientific Practice" Ross bemoans his inability to gain traction for Plex, writing of feeling "an intolerable burden of responsibility to still be the only person in the world (to my knowledge) pursuing it". His only recourse was to turn inward and "generate book after book on Plex in my office at home, in order that Plex will be ready when the world is ready for it!"

At this point, Doug Ross might be sounding a little bit like a crank. Let me be clear: Douglas T. Ross, computer science pioneer, was _absolutely_ a crank of the first water. This is just as absolutely to his credit; any fool can make it from the sublime to the ridiculous, but it takes real talent to go in the other direction. And Plex _is_ sublime, if in its own dry, academic way. Ross is not the celestial paranoiac Francis E. Dec, ranting and raving about the [Worldwide Deadly Gangster Communist Computer God](http://www.bentoandstarchky.com/dec/intro.htm) and lunar brain storage depots; nor is Plex the gonzo experience of [Nature's Harmonic Simultaneous 4-Day Time Cube](https://timecube.2enp.com/). That said, Ross never devolves into the racist vituperations Dec and Time Cube's Gene Ray were sometimes given to, either. So it goes.

->**⁂**<-

Plex itself is a sprawling, incoherent metaphysics built, according to Ross, on the foundation of a single pun (or, more properly, double entendre): "nothing can be left out". Thus inspired, Ross embarks upon the classic Cartesian thought experiment. But where Descartes discards every proposition except the cogito ("I think, therefore I am"), Ross's buck stops at "nothing doesn't exist".

Or, in Ross's own framing:

> **Nothing doesn't exist**. That is _the_ **First Definition** of Plex -- a scientific philosophy whose aim is _understanding our understanding of the nature of nature_. Plex does not attempt to understand nature _itself_, but only our _understanding_ of it. We are _included_ in nature as we do "our understanding", both scientific and informal, so we must understand _ourselves_ as well -- not just what we _think_ we are, but as we _really_ are, as _integral, natural **beings** of nature_. _How_ one "understand"s and even who "we" _are_ as we _do_ "our understanding" necessarily is left completely open, for all that must arise _naturally_ from the very _nature_ of nature.

All emphasis -- all of it, I assure you -- original. Ross's dedication to bold and italic text wavers from work to work and page to page, but on balance "From Scientific Practice to Epistemological Discovery" is in fine form. Early entries he refers to in his "thousands of C-pages" (that is, "chronological working pages", all of which may or may not have been lost) and [lecture notes he prepared in 1985](https://groups.csail.mit.edu/mac/projects/studentaut/lecture4/plex_lectures_book_ok.htm) sometimes switch between up to eight colors every few words. The lecture notes are of particular interest compared to the other extant materials, comprising a "study of an SADT Data Model which expresses all aspects of any object which obeys laws of physical cause and effect" delivered as a dialogue between Ross and a genie reminiscent of _Gödel, Escher, Bach_.

Having arrived at the First Definition, Ross next attempts to deduce everything else from it, claiming that Plex need make no assumptions. "Nothing doesn't exist" leads, expanded this way and that, to "Only that which is _known by definition_ is **known** -- by definition", as, "_without_ a definition for something, we only can know it as Nothing". Within the space of a few paragraphs, he's slammed what appears to be his own misinterpretation of Stephen Hawking and (unknowingly?) reinvented Spinoza's pantheism, on the grounds that "Nothing **isn't**; Plex is what Nothing **isn't**". And for what it's worth, this is all still in the first two pages of "From Scientific Practice".

->**⁂**<-

In another instance, Plex guides Ross to enlightenment regarding questions of information theory. It turns out that a single bit actually requires 3/2 binary digits for encoding, "because the value of the _half-bit_ is 3/4 !!!".

> -- which ultimately results from the fact that in _actuality_, when you don't have something, it is _not_ the case that you _have_ it _but_ it is Nothing -- it is that you **don't have** it; whereas when you _do_ have something, that is because you **don't have** what it _isn't_!

At a closer reading, this isn't necessarily the gibberish it might seem at first blush. Plex's foundation in "Nothing" makes `zero` the default state. But `one` is only understandable when there's an understood meaning for `one`. The elaboration about nothings and somethings makes it seem like Ross is counting this other `one` -- that is, half a bit -- towards the cost of encoding any other bit. In semiotic terms, this is the _interpretant_ or subjective value Charles Sanders Peirce sees implicit in signification. But if Ross ever investigated the ways logicians and linguists had already been exploring this territory, there's no indication that he attached any significance (as it were) to their work. And while including the interpretant for half the possible values may yield the same final figure, it does not account for the 3/4 half-bit; so in the face of storage hardware design as practiced, Ross's insistence on 3/2 seems more mystical than scientific.

I have no idea how _au courant_ Ross was with the humanities in general, but it seems likely that the answer is "not very". He was, of course, quite well-versed in math and engineering. Even deep in the mire of Plex, one can find him struggling to accommodate the realization that he was, in essence, defining formal systems backwards (he settles this with the ingenious maneuver of declaring the distinction akin to chirality), but the only philosopher he mentions is Plato. His efforts at deductive logic too seem thoroughly warped, as evinced by his "proof that every point is the whole world". For reference, an object's "identity" is tautologically defined as above: the set of "that" which "this" isn't.

```markdown
  I  n = 1: A world of one point is the whole world.
 II  Assume the theorem is true for (n - 1) points. (n > 1),
     i.e., for any collection of (n - 1) points, every point is the whole world.
     [ed: remember, Plex needs no assumptions, let alone "assume the theorem is true"]
III  To prove the theorem for n points given its truth for (n - 1) points
     (n > 1)
     (a) The identity of any one point, p, in the collection is a collection of (n -
         1) points, each of which is the whole world, by II.
     (b) The identity of any other point, q, i.e., a point of the identity of p, is
         a collection of (n - 1) points, each of which is the whole world, by II.
     (c) The identity of p and the identity of q are identical except that where
         the identity of p has q the identity of q has p. In any case p is the
         whole world by (b) and q is the whole world by (a).
     (d) Hence both p and q are the whole world, as are all the other points (if
         any) in their respective identities (and shared between them).
     (e) Hence all n points are the whole world.
 IV  For n = 2, I is used (via II) in IIIa and IIIb, q.e.d.
  V  Q.E.D. by natural induction.
```

As mentioned, Ross generated a wealth of C-pages, lecture notes, and other writings on Plex, but except for a small fraction apparently hosted on [his last MIT faculty/program page](https://groups.csail.mit.edu/mac/projects/studentaut/index.htm), I have no idea where most of this collection ended up. If you're interested in reading further in Ross's own words, the best places to start are probably "From Scientific Practice to Epistemological Discovery" in [_Software Development and Reality Construction_](https://www.researchgate.net/publication/242530010_Software_Development_and_Reality_Construction) or [The Plex Tract](https://groups.csail.mit.edu/mac/projects/studentaut/The%20Plex%20Tract.htm).

## Coda

Doug Ross himself remains a rather cryptic figure. There's some biographical information out there, but after his birth to missionary parents in what's now Guangdong and childhood homecoming to the Finger Lakes region of New York it mostly concerns where, when, with whom, and on what he was working. In his writings he comes off somewhat full of himself, as tends to be the case with esoteric philosophers and visionaries for whom the world is not yet and will never be ready. But when Ross talks about the necessary perfection, or perfect necessity, of his marriage to his wife Pat, herself a human computer at MIT's Lincoln Laboratory, it's still a little bit charming. And when he writes, with complete seriousness, that "being a pioneer came naturally" to him, I can't exactly say otherwise.

I wonder what it was like in that conference hall in 1988. I don't know whether the attendees or the organizers knew what they were in for when Ross got up to talk about this beautiful, all-consuming nonsense that was driving him to desperation. But sense isn't everything; and as a project of _reality construction_ Plex is a monumental accomplishment. And the reality we ourselves have collectively constructed, in which points are points, a bit corresponds to a single binary digit, and genies obstinately refuse to appear no matter how we manipulate bottles, is the richer for its existence.
