---
layout: Post
title: 'Populating GCE Persistent Disks'
date: 2017-11-14
tags:
  - cloud
  - infrastructure
---

This is one of those things I didn't expect to be quite this involved and couldn't find a step-by-step guide for. I just want to allocate a disk out there in The Cloud and put something on it, right? The idea being that then I spin up a cluster and my fancy application server container startup script pulls that static data off the disk and performs whatever initialization is required. It doesn't _sound_ especially complicated.

But as yet, there's no "upload some stuff" button in the Google Cloud Engine console, and `gcloud compute scp` only works for VM instances. There's nothing sitting in front of a Persistent Disk that will just pull your data into it. So, you have to create one:

1. Spin up a new VM instance in the Compute Engine screens. You'll need to be an `iam.serviceAccountUser` for the project.
2. Once it's running, log in:

    `gcloud compute --project "my-project" ssh --zone "us-central1-b" "my-new-instance"`
3. `lsblk` shows devices as usual. Find yours. Now format it (assuming it's a new disk):

    `sudo mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/sdwhatever`
4. Create a mountpoint directory and `sudo mount -o discard,defaults /dev/sdwhatever /mnt/my-mount-point`.
5. `gcloud compute scp local-file my-new-instance:remote-file`. You may have to get detailed with `--zone` flags if GCE can't determine which instance you're talking about.
6. Remember to clean up after yourself and `umount` the mountpoint!

Delete the instance from the VM Instances page. A GCE Persistent Disk can't be attached in read/write mode to more than one instance or container, also something to be mindful of if you're adding it to a deployment with multiple replicas.
