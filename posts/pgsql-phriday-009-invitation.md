---
layout: Post
title: "PGSQL Phriday #009 Invitation: Making Changes"
summary: "Friday June 2!"
date: 2023-05-26
tags:
  - postgresql
---

It's almost Phriday again! This is a monthly blogging event for the PostgreSQL community. [The rules](https://www.pgsqlphriday.com/rules/):

- publish something on-theme on or near **Friday, June 2nd**
- include "PGSQL Phriday #009" in your title or first paragraph, and link to this invitation post
- share it! The best way to reach the greater Postgresphere is to [get syndicated on Planet Postgres](https://planet.postgresql.org/add.html), but you can also share on [#pgsqlphriday in the community Slack](https://postgresteam.slack.com/archives/C044VV1T25S) or post to social media with the #PGSQLPhriday hashtag

This month's topic is **database change management**, aka schema evolution. I've been doing this in one form or another, using one framework or another (and on one less-memorable-than-you'd-think occasion writing my own in a thousand lines of [Ant](https://ant.apache.org) XML) for almost as long as I've worked in software. If you interact with databases in more than a read-only capacity, you've probably done your share of it as well. It's common, it's necessary, it's not very glamorous.

Every now and then, someone will extol the benefits of version-controlling your schema -- [Grant Fritchey](https://www.scarydba.com) discussed this at PGDay Chicago just last month -- or write a how-to for a specific framework. There's a [slow current of academic interest in the topic](http://se-pubs.dbs.uni-leipzig.de) which seems to have limited feedback into industry, publications tending toward the descriptive or the heavily specialized with only the occasional experiment like [PRISM](http://yellowstone.cs.ucla.edu/schema-evolution/index.php/Prism) seeing daylight. But the people deploying changes day to day don't tend to talk much about the nitty-gritty details or the experience of modifying a running database, because change management is plumbing.

<!-- Of the migrations we write, many more tend to be tedious and/or grueling than are elegant puzzles expressing an alchemic transformation of schema lead into schema gold. Practically all of them are incredibly proprietary, too. So we all know how to deploy changes our way, even if that way is yoloing DDL into production off-hours, fingers crossed in hope no-one notices; and we know everyone else knows how to do it _their_ way, or at least can read the same docs we have. Change management is plumbing. -->

Plumbing is _really important_, and there are a lot of fascinating technical, procedural, social, even philosophical aspects to it. Let's haul a few of them into the spotlight!

Some starting points:

- how does a change make it into production? Do you have a dev-QA-staging or similar series of environments it must pass through first? Who reviews changes and what are they looking for?
- what's different about modifying huge tables with many millions or billions of rows? How do you tackle those changes? Do you use the same strategy for smaller tables?
- how does Postgres make certain kinds of change easier or more difficult compared to other databases?
- do you believe that "rolling back" a schema change is a useful and/or meaningful concept? When and why, or why not?
- how do you validate a successful schema change? Do you have any useful processes, automated or manual, that have helped you track down problems with rollout, replication, data quality or corruption, and the like?
- what schema evolution or migration tools have you used? What did you like about them, what do you wish they did better or (not) at all?
- tales of terror in the [Kletzian mode](https://books.google.me/books?id=n2YA3FAv8DoC&lpg=PR4&pg=PR4#v=onepage&q&f=false) are also of course very welcome!
